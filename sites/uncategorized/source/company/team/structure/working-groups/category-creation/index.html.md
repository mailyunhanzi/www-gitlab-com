---
layout: markdown_page
title: "Category Creation Working Group"
description: "The business goal of GitLab category creation initiative is to solidify GitLab's 'The DevOps Platform’ messaging"
canonical_path: "/company/team/structure/working-groups/category-creation/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | 2021-07-20 |
| Estimated Date Ended   | 2021-10-31 |
| Slack        | [#wg-category-creation](https://app.slack.com/client/T02592416/C028G84V26S/thread/D012QPR06GP-1615593523.027100) (only accessible from within the company) |
| Google Doc   | Category Creation Working Group - Agenda 
 |


## Business Goal

Solidify GitLab's 'The DevOps Platform’ messaging, including messaging for:
1.  One sentence
1.  One pager
1.  Golden pitch
1.  Website
1.  'Airport banner' / Brand agency
1.  Category creation
1.  Commit keynote
1.  Key external audience messaging

## Exit Criteria

Add Epic


## Roles and Responsibilities

| Working Group Role    | Person                | 
|-----------------------|-----------------------|
| Facilitator           | Christine Lee         | 
| Functional Lead(s)    | Craig Mestel |
| Member                | Tony Righetti, Harsh Jawharkar, Melissa Smolensky, Stella Treas, Mike Pyle, Monica Galleto | 
| Executive Stakeholder | Sid Sijbrandij |

