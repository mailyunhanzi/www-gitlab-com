---
layout: handbook-page-toc
title: "Digital Customer Programs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Digital Programs journey 

This page focuses on emails and nurture programs, owned and managed by Customer Programs in Customer Success Operations. These programs are heavily focused on creating integrated “journeys” for key moments in the customer lifecycle. Our digital approach is designed to enable customers to better self-service the educational and best practices consulting that Technical Account Managers (TAMs) provide in the [TAM-Assigned segment](/handbook/customer-success/tam/customer-segments-and-metrics/#tam-assigned-segment).

Strategy deck can be found [here](https://docs.google.com/presentation/d/1EsCcVqKYL1WkwFkOZDr6TV_DBJMEvrGy3ErPlJLcfPg/edit?usp=sharing). (_GitLab internal only_)

## Digital Program content

Digital program content is an extension of our existing documentation and community enablement materials:

- [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg)
- [GitLab blog](https://about.gitlab.com/blog/)
- [Case studies](https://about.gitlab.com/customers/)
- [Forums](https://forum.gitlab.com/)
- [GitLab Docs](https://docs.gitlab.com/)
  - [Onboarding quick start](https://docs.gitlab.com/ee/administration/get_started.html)
  - [CI/CD quick start](https://docs.gitlab.com/ee/ci/quick_start/)

This content is designed to help organize the most relevant and useful resources into one location. Using advice taken from hundreds of existing customers’ experiences and unique environmental or business requirements, it will help implementation engineers quickly become experts.

![Customer Lifecycle](https://lucid.app/publicSegments/view/880a7c11-120e-4972-aa6f-972395cdbb3f/image.png "Customer Lifecycle")

## Email nurture programs

The email templates will use basic onboarding and enablement material provided by the Technical Account Management Commercial segment, and Technical Writing team. The content created will live on our Docs site which will be the single source of information. ⭐

- [Convert Emails to Docs Epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/73)

### Onboarding resources

The Onboarding Quick Guide project template will allow customers to self-service the materials provided from the email templates. The following video provides an overview of how to use the Onboarding Quick Guide project template.

[![GitLab Onboarding Quick Guide](https://i.imgur.com/p3ZPslJ.png
)](https://youtu.be/I0_VMLNpA_A "GitLab Onboarding Quick Guide")

- [Onboarding Quick Guide project template](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/onboarding-quick-guide)

- [Onboarding Enablement](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/320)

- [Epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/65)

- [Email copy for post-onboarding survey](https://docs.google.com/document/d/1B3RV2RuUkb3RzuQeNUTDz1BnpZLRwInnb_igm4ra7aw/edit?usp=sharing)

### Stage enablement 

- [CI (Verify) Enablement](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/190)

- [DevSecOps (Secure) Enablement](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/319)

- [CD (Release) Enablement](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/348)

### Newsletters

- [Email copy of past and upcoming newsletters](https://docs.google.com/document/d/1Fv8B49E7FSmKDgc6uDQG51p3UAj-OsgwmdPtdAyRKcc/edit?usp=sharing)

### Webinars

- [Customer Programs Commercial Webinar Series](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/37)

### NPS/CSAT surveys

Information on customer NPS/CSAT surveys can be found on the [Customer NPS/CSAT surveys page](https://about.gitlab.com/handbook/customer-success/tam/gainsight/#customer-npscsat-surveys).

- [NPS/CSAT Survey issue for all customers](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/228)

## How to access and use Digital Programs

You may request new Digital Programs or update existing programs.

### Add contacts to Digital Programs journey

 [Nominating contacts for the digital journey](/handbook/customer-success/tam/digital-journey/nominating-contacts-for-the-digital-journey/)

### Create a CTA with a playbook

Program emails intended to be sent as a sequence have been added as [playbooks](/handbook/customer-success/tam/gainsight/#ctas) in Gainsight. These need to be sent manually by the TAM, but they are setup to facilitate reminders and make this process easy, while we learn how customers want to engage. To use a playbook to send emails:

1. In Gainsight, create a new CTA in the Customer 360.
1. For `Type`, choose **Digital Journey**.
1. Select the appropriate playbook. For example: New Customer Digital Onboarding - Self-Managed Email Series, which will add a checklist for each email in the sequence.
1. Save the CTA.
1. Click the whitespace next to the name of the next email to be sent.
1. Click **Validate Email**. This opens an editor where you can choose contacts and modify the email before sending it.
1. Click **Preview and Proceed** to see the fully populated and formatted email before sending.
1. Send the email, or save as draft.  

### Request a new Digital Program journey
 
To request new programs or make updates to an existing program:

1. Open an issue in the [CS Ops project](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues).
1. Apply the program request or new program template.
1. Make an `@mention` to a Gainsight administrator.

This information from the issue template **must** be filled out before the program will be created:

- Goal of this program: _Please be as specific as possible, how does this tie into OKRs and link to epics when available._
- Intended outcome: _Requester to determine what metrics (success criteria) apply. Please document the intended outcome and how it will be measured._
- Requested launch date/time
- Participant criteria included, OR attach a CSV List. _Reference the filtering fields table below for criteria._
- Sender name: _Typically we tokenize this to come from either a TAM or AE for most outgoing communication. For a more generic send we use customer_enablement@gitlab.com._
- Sender email address: _What email address should be used as the sender?_
- Provide final copy: _This is the content of the email you wish to send. Please include as much information, including links, as possible._

2. Once the program is created and ready to launch it will be communicated in the issue followed by metrics to demonstrate success/failures. For example:

- Open rates
- Click rates
- Unsubscribes
- Product analytics
- Seat utilization

